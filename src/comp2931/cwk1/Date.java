// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {
  private int year;
  private int month;
  private int day;
    public static Environment env = new SystemEnvironment();

    public static void load(Environment newEnv) {
        env = newEnv;
    }

    public static Date getDate() {
        return env.getDate();
    }

    /**
     * default constructor
     */
    public Date() {
    }

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
      set(y,m,d);
  }
  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }

    /**
     * Tests whether this date is equal to another.
     *
     * <p>The two objects are considered equal if both are instances of
     * the Date class <em>and</em> both represent exactly the same
     * time of day.
     *
     * @return true if this Date object is equal to the other, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) {
            // 'other' is same object as this one
            return true;
        } else if (!(other instanceof Date)) {
            // 'other' is not a Date object
            return false;
        } else {
            // Compare fields
            Date otherDate = (Date) other;
            return getYear() == otherDate.getYear()
                    && getMonth() == otherDate.getMonth()
                    && getDay() == otherDate.getDay();
        }
    }

    /**
     * Provides a method to validate input and set values
     *
     * @param y Year
     * @param m Month
     * @param d Day
     *
     * validate year, month, day
     *
     *throw IllegalArgumentException while the input is invalid.
     *
     * set y,m,d to year, month, day
     */
    private void set(int y, int m, int d) {

        if (y < 0 || y > 9999) {
            throw new IllegalArgumentException("years out of range");
        }
        else if (m < 1 || m > 12) {
            throw new IllegalArgumentException("months out of range");
        }
        else if (d < 1 || (d > 29 & m == 2 &y%4==0)|| (d > 28 & m == 2 &y%4!=0)||(d>31)||(d>30&(m==4||m==6||m==9||m==11))) {
            throw new IllegalArgumentException("days out of range");
        }
        else {
            year = y;
            month = m;
            day = d;
        }
    }
    /**
     * Provides a method to return the day of the year
     *
     * Create a calendar instance
     *
     *set the date with year,month,day
     *
     * @return DAY_OF_YEAR
     */
    public int getDayOfYear(){
        Calendar theDate = Calendar.getInstance();
        theDate.set(year,month-1,day);
        return theDate.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * an interface for creating a today date
     */
    public interface Environment {
        Date getDate();
    }

    /**
     * implements the interface Environment
     * <p>
     * using Calendar to get today's year, month, day
     *
     * @return real today's date
     */
    public static class SystemEnvironment implements Environment {
        public Date getDate() {
            Calendar calendar = Calendar.getInstance();
            int y = calendar.get(Calendar.YEAR);
            int m = calendar.get(Calendar.MONTH) + 1;
            int d = calendar.get(Calendar.DATE);
            Date today = new Date(y, m, d);
            return today;
        }
    }

    /**
     * implements the interface Environment
     * <p>
     * set a mock date for testing
     *
     * @return mock today's date
     */
    public static class MockEnvironment implements Environment {
        private Date currentTime = new Date(2017, 11, 9);
        public Date getDate() {
            return currentTime;
        }
        public void setTime(Date mockD) {
            currentTime = mockD;
        }
    }

    /**
     * a method to produce a today's date whatever real or mock
     */
    public void doToday() {
        Date today = Date.getDate();
    }
}
