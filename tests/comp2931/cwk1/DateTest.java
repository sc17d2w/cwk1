package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest {
    private Date newYear;
    private Date endYear;
    private Date Christmas;
    private Date leapDay;
    private Date today;
    private Date.Environment env;

    public void doTodayAPI() {
        today.doToday();
    }
    @Before
    public void setUp() {
        newYear = new Date(2017, 1, 1);
        endYear = new Date(2017, 12, 31);
        Christmas = new Date(2017, 12, 25);
        leapDay = new Date(2016,2,29);
        today = new Date();
        env = new Date.MockEnvironment();
    }

    @Test
    public void dateToString() throws Exception {
        assertThat(newYear.toString(), is("2017-01-01"));
        assertThat(endYear.toString(), is("2017-12-31"));
        assertThat(Christmas.toString(), is("2017-12-25"));
        assertThat(leapDay.toString(), is("2016-02-29"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void yearTooHigh() {
        new Date(10000, 12, 30);
    }

    @Test(expected=IllegalArgumentException.class)
    public void yearTooLow() {
        new Date(-1, 12, 30);
    }

    @Test(expected=IllegalArgumentException.class)
    public void monthTooHigh() {
        new Date(2017, 13, 30);
    }

    @Test(expected=IllegalArgumentException.class)
    public void monthTooLow() {
        new Date(2017, 0, 30);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooHigh() {
        new Date(2017, 11, 31);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooLow() {
        new Date(2017, 12, 0);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooLowInLeapYear() {
        new Date(2016, 12, 0);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooHInLeapYear() {
        new Date(2016, 2, 30);
    }

    @Test
    public void equalsTest(){
        assertTrue(newYear.equals(newYear));
        assertTrue(newYear.equals(new Date(2017,1,1)));
        assertFalse(newYear.equals(endYear));
        assertFalse(newYear.equals(new Date(2017,1,11)));
        assertFalse(newYear.equals(new Date(2018,1,1)));
        assertFalse(newYear.equals(new Date(2017,11,1)));
    }

    @Test
    public void getDayOfYearTest()  {
        assertTrue(newYear.getDayOfYear()==1);
        assertTrue(endYear.getDayOfYear()==365);
        assertFalse(newYear.getDayOfYear()==2);
        assertFalse(newYear.getDayOfYear()==366);
        assertThat(Christmas.getDayOfYear(),is(359));
        assertThat(new Date(2017,11,8).getDayOfYear(),is(312));
    }

    @Test
    public void leapYearsTest(){
        assertThat(new Date(2016,11,8).getDayOfYear(),is(313));
        assertThat(leapDay.getDayOfYear(), is(60));
        assertThat(new Date(2012,12,31).getDayOfYear(),is(366));
    }

    @Test
    public void defaultConstructorTest() {
        int year = 2017;
        int month = 11;
        int day = 9;
        Date fakeDay = new Date();
        assertTrue(fakeDay.equals(new Date(year, month, day)));
    }

    @Test
    public void defaultDateTestUseMock() {
        Date.Environment mockEnv = new Date.MockEnvironment();
        Date.load(mockEnv);
        today.doToday();
        assertTrue(today.toString().equals("2017-11-09"));
    }
}